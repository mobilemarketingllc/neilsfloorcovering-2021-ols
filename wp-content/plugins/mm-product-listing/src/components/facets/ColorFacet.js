import React from "react";

export default function ColorFacet({ handleFilterClick, productColors }) {
  function sortObject(obj) {
    return Object.keys(obj)
      .sort()
      .reduce((a, v) => {
        a[v] = obj[v];
        return a;
      }, {});
  }
  productColors = sortObject(productColors);
  return (
    <div class="facet-wrap  facet-display">
      <strong>Colors</strong>
      <div className="facetwp-facet">
        {Object.keys(productColors).map((color, i) => {
          if (color && productColors[color] > 0) {
            return (
              <div>
                <span
                  id={`color-filter-${i}`}
                  key={i}
                  data-value={`${color.toLowerCase()}`}
                  onClick={(e) =>
                    handleFilterClick("color_facet", e.target.dataset.value)
                  }>
                  {" "}
                  {color} {` (${productColors[color]}) `}
                </span>
              </div>
            );
          }
        })}
      </div>
    </div>
  );
}
